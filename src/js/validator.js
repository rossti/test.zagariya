$(document).ready(function () {
    $('#form-data').hide();
    $('.form-content').show();

    var formData;

    $(function(){
        $('#form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) { // it means form is valid
                e.preventDefault();// stop send form data
                formData = $(this).serializeArray(); // save form data in variable
                signInHandler();// call function, which sends data in block $('.form-data')
            }

        });
    });

    function signInHandler() {
        $('.form-content').hide();
        $('#form-data').show();
        for (var i = 0; i < formData.length; i++) {
            $('#form-data').append
            ('<span class="form-item">' + formData[i].name + ' - ' + formData[i].value +'</span>')
        }
    }


});